package mcts

type SearchHeap []*SearchNode

func (a SearchHeap) Len() int           { return len(a) }
func (a SearchHeap) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a SearchHeap) Less(i, j int) bool { return a[i].Priority.Priority() > a[j].Priority.Priority() }

// For heap.Interface (not used in Search).
func (a *SearchHeap) Push(x any) { *a = append(*a, x.(*SearchNode)) }
func (a *SearchHeap) Pop() any   { i := len(*a) - 1; x := (*a)[i]; *a = (*a)[:i]; return x }
