package mcts

import (
	"container/heap"
	"math"
	"sync"
)

type Node interface {
	Values() Values
	Moves() []Edge
	Apply(Edge) Node
}

type Label = int

type LabelProvider interface {
	Label(Label) string
}

type Values interface {
	Value(Label) float64
}

type UpdateValues interface {
	Values
	AddWeight(Values)
}

type FloatValues map[Label]float64

func (v FloatValues) Value(x Label) float64 { return v[x] }
func (v FloatValues) AddWeight(w Values) {
	for x := range v {
		v[x] += w.Value(x)
	}
}

type Priority interface {
	Priority() float64
}

type UpdatePriority interface {
	Priority
	UpdatePriority(w Values, runs float64)
}

type UCTPriority struct {
	dirty    bool
	rootRuns float64
	runs     float64
	explore  float64
	labels   []Label
	weight   UpdateValues
	priority float64
	once     sync.Once
}

func (x *UCTPriority) init() {
	x.dirty = true
	x.explore = math.Pi
	x.weight = make(FloatValues)
}

func (x *UCTPriority) computePriority() {
	x.priority = 0
	for _, l := range x.labels {
		x.priority += x.weight.Value(l)/x.runs + x.explore*math.Sqrt(math.Log(x.rootRuns+x.runs)/x.runs)
	}
	if n := len(x.labels); n > 1 {
		x.priority /= float64(n)
	}
	x.dirty = false
}

func (x *UCTPriority) UpdatePriority(w Values, runs float64) {
	x.once.Do(x.init)
	x.dirty = true
	x.weight.AddWeight(w)
	x.rootRuns += runs
	x.runs += runs
}

func (x *UCTPriority) Priority(l Label) float64 {
	x.once.Do(x.init)
	if x.dirty {
		x.computePriority()
	}
	return x.priority
}

type Edge interface {
	Labels() []Label
	String() string
}

type SearchNode struct {
	Node
	Edges    []Edge
	Children SearchHeap
	Values   Values

	Root          *SearchNode
	RootMoveIndex int

	Weight   UpdateValues
	Priority UpdatePriority
	Runs     float64

	Options *SearchOptions
}

func NewRootSearchNode(root Node) *SearchNode {
	return &SearchNode{
		Node:   root,
		Edges:  root.Moves(),
		Values: root.Values(),
	}
}

func (n *SearchNode) NewSearchNode(rootMoveIndex int) *SearchNode {
	x := n.Apply(n.Edges[rootMoveIndex])
	return &SearchNode{
		Node:          x,
		Edges:         x.Moves(),
		Values:        x.Values(),
		Root:          n,
		RootMoveIndex: rootMoveIndex,
	}
}

func (n *SearchNode) generateChildren() {
	if n.Children != nil {
		panic("children already generated")
	}
	n.Children = make(SearchHeap, 0, len(n.Edges))
	for i := range n.Edges {
		n.Children = append(n.Children, n.NewSearchNode(i))
	}
}

func (n *SearchNode) RootRuns() float64 {
	if n == nil || n.Root == nil {
		return 0
	}
	return n.Root.Runs
}

func (n *SearchNode) SampleChild() *SearchNode {
	if n.Children == nil {
		n.generateChildren()
	}
	if len(n.Children) == 0 {
		return nil
	}
	return n.Children[0]
}

func (n *SearchNode) BackpropWeights(w Values, runs float64) {
	if runs < 0 {
		panic("AddWeight called with runs < 0")
	}
	for ; n != nil; n = n.Root {
		n.Weight.AddWeight(w)
		n.Runs += runs
		// UCT (Upper Confidence Bound 1 applied to trees)
		n.Priority.UpdatePriority(w, runs)
		if root := n.Root; root != nil {
			heap.Fix(&root.Children, n.RootMoveIndex)
		}
	}
}

type SearchOptions struct {
	NumRollouts          int
	ExplorationParameter float64
}

func DefaultSearchOptions() *SearchOptions {
	return &SearchOptions{
		NumRollouts:          100,
		ExplorationParameter: math.Pi,
	}
}

type SearchResult struct {
	Root *SearchNode
}

func Search(root Node, opts *SearchOptions) *SearchResult {
	if root == nil {
		return nil
	}
	rootNode := NewRootSearchNode(root)
	if opts == nil {
		opts = DefaultSearchOptions()
	}
	for i := 0; i < opts.NumRollouts; i++ {
		n := rootNode
		for next := n.SampleChild(); next != nil; next = n.SampleChild() {
			n = next
		}
		n.BackpropWeights(n.Values, 1)
	}
	return &SearchResult{
		Root: rootNode,
	}
}
